module github.com/koala7659/noter/server

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/vrischmann/envconfig v1.3.0
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
