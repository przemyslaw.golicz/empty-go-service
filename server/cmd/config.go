package main

import (
	"fmt"
)

type config struct {
	Storage  string `envconfig:"default=memory"`
	LogLevel string `envconfig:"default=info"`
}

func (c *config) String() string {
	return fmt.Sprintf("Storage: %s, LogLevel: %s", c.Storage, c.LogLevel)
}