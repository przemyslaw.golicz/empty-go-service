package main

import (
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
	"net/http"
)

func main() {

	cfg := config{}
	err := envconfig.InitWithPrefix(&cfg, "APP")
	exitOnError(err, "Failed to load application config")

	logLevel, err := log.ParseLevel(cfg.LogLevel)
	if err != nil {
		log.Warnf("Invalid log level: '%s', defaulting to 'info'", cfg.LogLevel)
		logLevel = log.InfoLevel
	}
	log.SetLevel(logLevel)

	log.Infof("Starting noter application")
	log.Infof("Config: %s", cfg.String())

	r := mux.NewRouter()

	r.HandleFunc("/api/hello", getHello). Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", r))
}

func getHello(w http.ResponseWriter, r *http.Request){
	log.Infof("get Hello world called")
	w.Write([]byte("Hello world!"))
}


func exitOnError(err error, context string) {
	if err != nil {
		wrappedError := errors.Wrap(err, context)
		log.Fatal(wrappedError)
	}
}