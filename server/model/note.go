package model

type Note struct {
	ID   string `json:"id"`
	Body string `json:"body"`
	User string `json:"user-id"`
	TTL  int    `json:"ttl"`
}

type NoteStorage interface {
	GetNote(id string, userId string) (Note, error)
	GetAllUserNotes(userId string) ([]Note, error)
	DeleteNote(id string, userId string) error
	AddNote(note Note, userId string) error
}
